Demo Application
================

Create an image gallery application that allows a user to view a set of
images. Requirements:
- Display a series of images (perhaps as thumbnails, or similar size) together on the screen.
- The user can click an image and that image will be brought into focus as a larger, full size image.
- An image that is that is �in focus� / enlarged should show a list of comments.
- At the bottom of the list of comments, the user has the ability to leave a comment.
- A comment is composed of text, a date-time formatted like "Feb 17, 2016at 10:15am", and a user name.
- After a user submits a new comment, save it to the server (This doesn't need a backend. Just make an AJAX call to a dummy URL).
- Also save that comment in an Angular service or factory associated with that image.
- The comments must persist when a user selects a new image. (in the factory/service, since we don�t really have a backend to pull them from).
- Use only Angular (version of your choice) and vanilla JS.

What we are looking for:
- Application structure (Use of services, controllers, etc...)
- JavaScript best practices
- Clarity / readability
- Separation of concerns
- Reasonable error handling
- Reasonable commenting you would expect a good developer to leave for future maintainers
- Adherence to a consistent style guide of your choice, or just your own consistent naming / structure / organization.
- Performance� if we add 100 images to your app, what happens? 1,000 images? 10,000?


What we are not looking for:
- CSS is less important than the application code. We just want it to be
useable� you don�t need to spend time with animated transitions, fancy
shading, etc etc.
- excessive documentation / packaging. No need to create a MD doc / npm
repo / package.json / etc etc.
- over engineering, use of local storage, analytics, specs, cookies use.
- Please also pre-compile the app into a zip that can be extracted.