(function (){

    var app = angular.module ('gallery-Directives',[]);

    app.directive('instagramImages',['GalleryFactory', function(GalleryFactory) {
        return {
            restrict: 'E',
            template: '<div ng-controller="GalleryController">'+
            '<div class="container" ><div  class = "row" >'+
            '<div class="three columns" ng-repeat =" item in gallery">'+
            '<img ng-src = "{{item.images.thumbnail.url}}" ng-click="openModal($index)" > </div> </div> </div>'
        }
    }]);

    app.directive('modalBox', function() {
        return {
            restrict: 'E',
            scope: {
                show: '='
            },
            replace: true, // Replace with the template below
            transclude: true, // we want to insert custom content inside the directive
            link: function(scope, element, attrs) {
                scope.dialogStyle = {};
                    scope.dialogStyle.width = '90%';
                    scope.dialogStyle.height = '90%';
                scope.closeModal = function() {
                    scope.show = false;
                };
            },
            template: '<div class="ng-modal" ng-show="show"><div class="ng-modal-overlay" ng-click="closeModal()"></div>' +
            '<div class="ng-modal-dialog" ng-style="dialogStyle"><div class="ng-modal-close" ng-click="closeModal()">X</div>' +
            '<div class="ng-modal-dialog-content" ng-transclude></div></div></div>'
        };
    });


}) ();