/**
 * Created by zoheviCleef on 3/8/16.
 */


(function () {
    var app = angular.module('tdGallery', ['gallery-Directives']);

    app.factory('GalleryFactory', function () {
        var gallery = [];
        var gallery_comments = [];
        return {
            galleryItems: function () {
                return gallery[0];
            },
            addToGallery: function (galleryItems) {
                gallery.push(galleryItems);
            },
            getComments: function (index) {

                    if (gallery[0][index][gallery_comments]){
                    return gallery[0][index][gallery_comments];
                } else {
                    gallery[0][index].push(gallery_comments);
                    return gallery[0][index][gallery_comments];
                }
            },
            //addComments: function (index, userComments) {
            //    console.log(gallery);
            //    if (gallery[0].index.gallery_Comments) {
            //        console.log("creating comments arrays")
            //        gallery[0][index].push(gallery_comments[userComments]);
            //    } else {
            //        console.log("adding comments")
            //        gallery[0][index].push(gallery_comments);
            //        gallery[0][index].push(gallery_comments[userComments]);
            //    }
            //}
        };
    });

    
    app.controller('GalleryController', ['$scope', '$http', 'GalleryFactory', function ($scope, $http, GalleryFactory) {
        var gallery = [];
        var jsonUrl = 'https://api.instagram.com/v1/tags/cars/media/recent?client_id=5f9365e9f1054aa991726d731c65aa02&max_tag_id=1201728601152956236';
        $http({
            method: 'GET',
            url: jsonUrl
        }).then(function successCallback(response) {
            console.log("got JSON");
            gallery.push(response.data.data);
            GalleryFactory.addToGallery(gallery[0]);

            $scope.gallery = gallery[0];
        }, function errorCallback(response) {
            alert('Well, life happens!  Try getting a CORS extension for your browser .')
        });
        $scope.selectedItem = '';
        $scope.getSelectedItem = function () {
            return $scope.selectedItem;
        };
        $scope.setSelectedItem = function (itemIndex) {
            $scope.selectedItem = itemIndex;
        };
        $scope.showModal = false;
        $scope.openModal = function (itemIndex) {
            $scope.setSelectedItem(itemIndex);
            $scope.showModal = !$scope.showModal;
        };
    }]);


    app.controller('CommentController', [ '$scope', '$http', 'GalleryFactory', function($scope, $http, GalleryFactory){

        this.form = $scope.form;
        $scope.comments = function(){
            $scope.getComments(  $scope.getSelectedItem());
        };

        $scope.currentDate  = new Date();

        // Vanilla JS , no serealize w/ Jquery
        var data = {
            json: JSON.stringify({
           comment:this.form
            })
        };

        $scope.submitForm= function(){
            //addComments(  $scope.getSelectedItem(), JSON.stringify(this.form));

        $http({
            method: 'POST',
            url: "/echo/json/",
            data: data
        }).then(function successCallback(response) {
            console.log("Sent JSON");

        }, function errorCallback(response) {
            alert('Well, life does happens! Come back later, your opinion matters')
        });

        };

    }]);

})();